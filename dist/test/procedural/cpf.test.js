"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const cpf_1 = require("../../src/procedural/cpf");
test("Should validate a correct cpf with rest of division more than 2", function () {
    const isValid = (0, cpf_1.validate)("608.670.319-13");
    expect(isValid).toBeTruthy();
});
test("Should validate a cpf with rest of division less than 2 first digit", function () {
    const isValid = (0, cpf_1.validate)('956.313.804-04');
    expect(isValid).toBeTruthy();
});
test("Should validate a cpf with rest of division less than 2 second digit", function () {
    const isValid = (0, cpf_1.validate)('397.072.642-50');
    expect(isValid).toBeTruthy();
});
test("Should validate a incorrect cpf", function () {
    const isValid = (0, cpf_1.validate)("111.444.777-05");
    expect(isValid).toBeFalsy();
});
test("Should validate a incorrect cpf  cpf.length < 11", function () {
    expect(() => (0, cpf_1.validate)("444")).toThrow(new Error("Invalid Input: String Lenght incorrect"));
});
test("Should validate a incorrect cpf  cpf.length > 14", function () {
    expect(() => (0, cpf_1.validate)("11111111111111111111111")).toThrow(new Error("Invalid Input: String Lenght incorrect"));
});
test("Should validate a null ", function () {
    expect(() => (0, cpf_1.validate)(null)).toThrow(new Error("Invalid Input: Not a String"));
});
test("Should validate a undefined", function () {
    expect(() => (0, cpf_1.validate)(undefined)).toThrow(new Error("Invalid Input: Not a String"));
});
test("Should validate a cpf all equal not dot and dash", function () {
    expect(() => (0, cpf_1.validate)('11111111111')).toThrow(new Error("Invalid Input: All Cpf Number are Equal"));
});
test("Should validate a cpf all equal with dot and dash", function () {
    expect(() => (0, cpf_1.validate)('000.000.000-00')).toThrow(new Error("Invalid Input: All Cpf Number are Equal"));
});
test("Should validate a cpf all equal with dot and dash", function () {
    expect(() => (0, cpf_1.validate)('%%%.&&&.***-00')).toThrow(new Error("Invalid Input: Something other than Dash, Dot, Number, or Space"));
});
