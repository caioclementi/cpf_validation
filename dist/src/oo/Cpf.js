"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Cpf {
    constructor(cpf) {
        this.cpf = cpf;
        if (!this.isString(cpf))
            throw new Error("Invalid Input: Not a String");
        if (!this.isInRangeCpfLenght(cpf))
            throw new Error("Invalid Input: String Lenght Incorrect");
        if (!this.isInOnlyValidChars(cpf))
            throw new Error("Invalid Input: Not Only Valid Chars");
        this.cpfCleaned = this.cleanCpfString(cpf);
        if (this.isCpfStringWithSameNumber(this.cpfCleaned))
            throw new Error("Invalid Input: All Cpf Number are Equal");
    }
    isString(cpf) {
        return typeof (cpf) == 'string';
    }
    isInRangeCpfLenght(cpf) {
        return (cpf.length >= 11 && cpf.length <= 14);
    }
    isInOnlyValidChars(cpf) {
        const NotDashDotNumberSpaceRegex = /[^\-\.0-9 ]/gi;
        const InvalidCharArray = cpf.match(NotDashDotNumberSpaceRegex);
        return (InvalidCharArray == null);
    }
    cleanCpfString(cpf) {
        const DotSpacesDashRegex = /[\.\- ]/gi;
        return cpf.replace(DotSpacesDashRegex, '');
    }
    isCpfStringWithSameNumber(cpf) {
        return (cpf.split("").every(c => c === cpf[0]));
    }
    CpfDigitAlgorithm() {
        let cpfConverted = [];
        const chars = this.cpfCleaned.split('');
        chars.forEach((char) => {
            cpfConverted.push(parseInt(char));
        });
        const CpfWithoutCheckDigit = cpfConverted.slice(0, -2);
        const FirstCheckDigitSum = CpfWithoutCheckDigit.reduce((previousValue, currentValue, currentIndex) => previousValue + currentValue * (10 - currentIndex), 0);
        const FirstCheckDigitRemaider = FirstCheckDigitSum % 11;
        const FirstCheckDigit = (FirstCheckDigitRemaider < 2) ? 0 : 11 - FirstCheckDigitRemaider;
        const SecondCheckDigitSum = [...CpfWithoutCheckDigit, FirstCheckDigit].reduce((previousValue, currentValue, currentIndex) => previousValue + currentValue * (11 - currentIndex), 0);
        const SecondCheckDigitRemaider = SecondCheckDigitSum % 11;
        const SecondCheckDigit = (SecondCheckDigitRemaider < 2) ? 0 : 11 - SecondCheckDigitRemaider;
        return parseInt(this.cpfCleaned.at(-2)) === FirstCheckDigit && parseInt(this.cpfCleaned.at(-1)) === SecondCheckDigit;
    }
}
exports.default = Cpf;
