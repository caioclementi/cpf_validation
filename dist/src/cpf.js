"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validate = void 0;
function validate(cpfInput) {
    if (typeof (cpfInput) !== 'string')
        throw new Error("Invalid Input: Not a String");
    isInRangeCpfLenght(cpfInput);
    isAValidCpfString(cpfInput);
    let cpf = cleanCpfString(cpfInput);
    isCpfStringWithSameNumber(cpf);
    return CpfDigitAlgorithm(cpf);
}
exports.validate = validate;
function isInRangeCpfLenght(cpf) {
    if (cpf.length <= 10 || cpf.length >= 15)
        throw new Error("Invalid Input: String Lenght incorrect");
    return true;
}
function isAValidCpfString(cpf) {
    const NotDashDotNumberSpaceRegex = /[^\-\.0-9 ]/gi;
    const isInSomethingElse = cpf.match(NotDashDotNumberSpaceRegex);
    if (isInSomethingElse)
        throw new Error("Invalid Input: Something other than Dash, Dot, Number, or Space");
    return true;
}
function cleanCpfString(cpf) {
    const DotSpacesDashRegex = /[\.\- ]/gi;
    return cpf.replace(DotSpacesDashRegex, '');
}
function isCpfStringWithSameNumber(cpf) {
    if (cpf.split("").every(c => c === cpf[0]))
        throw new Error("Invalid Input: All Cpf Number are Equal");
    return true;
}
function CpfDigitAlgorithm(cpf) {
    let cpfConverted = [];
    const chars = cpf.split('');
    chars.forEach((char) => {
        cpfConverted.push(parseInt(char));
    });
    const CpfWithoutCheckDigit = cpfConverted.slice(0, -2);
    const FirstCheckDigitSum = CpfWithoutCheckDigit.reduce((previousValue, currentValue, currentIndex) => previousValue + currentValue * (10 - currentIndex), 0);
    const FirstCheckDigitRemaider = FirstCheckDigitSum % 11;
    const FirstCheckDigit = (FirstCheckDigitRemaider < 2) ? 0 : 11 - FirstCheckDigitRemaider;
    const SecondCheckDigitSum = [...CpfWithoutCheckDigit, FirstCheckDigit].reduce((previousValue, currentValue, currentIndex) => previousValue + currentValue * (11 - currentIndex), 0);
    const SecondCheckDigitRemaider = SecondCheckDigitSum % 11;
    const SecondCheckDigit = (SecondCheckDigitRemaider < 2) ? 0 : 11 - SecondCheckDigitRemaider;
    return parseInt(cpf.at(-2)) === FirstCheckDigit && parseInt(cpf.at(-1)) === SecondCheckDigit;
}
