export default class Cpf {
    cpfCleaned: string;

    constructor(readonly cpf: string) {
        if (!this.isString()) throw new Error("Invalid Input: Not a String");
        if (!this.isInRangeCpfLenght()) throw new Error("Invalid Input: String Lenght Incorrect");
        if (!this.isInOnlyValidChars()) throw new Error("Invalid Input: Not Only Valid Chars");
        this.cpfCleaned = this.cleanCpfString()
        if (this.isCpfStringWithSameNumber(this.cpfCleaned)) throw new Error("Invalid Input: All Cpf Number are Equal");
    }

    isString() {
        return typeof (this.cpf) == 'string';
    }

    isInRangeCpfLenght() {
        return (this.cpf.length >= 11 && this.cpf.length <= 14);
    }

    isInOnlyValidChars() {
        const NotDashDotNumberSpaceRegex = /[^\-\.0-9 ]/gi;
        const InvalidCharArray = this.cpf.match(NotDashDotNumberSpaceRegex);
        return (InvalidCharArray == null)
    }

    cleanCpfString() {
        const DotSpacesDashRegex = /[\.\- ]/gi;
        return this.cpf.replace(DotSpacesDashRegex, '');
    }

    isCpfStringWithSameNumber(cpf: string) {
        return (cpf.split("").every(c => c === cpf[0]))
    }

    CpfDigitAlgorithm() {
        let cpfConverted: number[] = []
        const chars = this.cpfCleaned.split('');
        chars.forEach((char) => {
            cpfConverted.push(parseInt(char));
        });
        const CpfWithoutCheckDigit = cpfConverted.slice(0, -2)
        const FirstCheckDigitSum: number = CpfWithoutCheckDigit.reduce((previousValue, currentValue, currentIndex) => previousValue + currentValue * (10 - currentIndex), 0);
        const FirstCheckDigitRemaider: number = FirstCheckDigitSum % 11;
        const FirstCheckDigit: number = (FirstCheckDigitRemaider < 2) ? 0 : 11 - FirstCheckDigitRemaider;
        const SecondCheckDigitSum: number = [...CpfWithoutCheckDigit, FirstCheckDigit].reduce((previousValue, currentValue, currentIndex) => previousValue + currentValue * (11 - currentIndex), 0);
        const SecondCheckDigitRemaider: number = SecondCheckDigitSum % 11
        const SecondCheckDigit: number = (SecondCheckDigitRemaider < 2) ? 0 : 11 - SecondCheckDigitRemaider;
        return parseInt(this.cpfCleaned.at(-2)!) === FirstCheckDigit && parseInt(this.cpfCleaned.at(-1)!) === SecondCheckDigit;
    }
}