export function validate(cpfInput: any) {
    if (typeof (cpfInput) !== 'string') throw new Error("Invalid Input: Not a String");
    isInRangeCpfLenght(cpfInput);
    isAValidCpfString(cpfInput);
    let cpf = cleanCpfString(cpfInput);
    isCpfStringWithSameNumber(cpf);
    return CpfDigitAlgorithm(cpf);
}

function isInRangeCpfLenght(cpf: String) {
    if (cpf.length <= 10 || cpf.length >= 15)
        throw new Error("Invalid Input: String Lenght incorrect");
    return true;
}

function isAValidCpfString(cpf: String) {
    const NotDashDotNumberSpaceRegex = /[^\-\.0-9 ]/gi;
    const isInSomethingElse = cpf.match(NotDashDotNumberSpaceRegex);
    if (isInSomethingElse)
        throw new Error("Invalid Input: Something other than Dash, Dot, Number, or Space");
    return true;
}

function cleanCpfString(cpf: String) {
    const DotSpacesDashRegex = /[\.\- ]/gi;
    return cpf.replace(DotSpacesDashRegex, '');
}

function isCpfStringWithSameNumber(cpf: String) {
    if (cpf.split("").every(c => c === cpf[0]))
        throw new Error("Invalid Input: All Cpf Number are Equal");
    return true;
}

function CpfDigitAlgorithm(cpf: string) {
    let cpfConverted: number[] = []
    const chars = cpf.split('');
    chars.forEach((char) => {
        cpfConverted.push(parseInt(char));
    });
    const CpfWithoutCheckDigit = cpfConverted.slice(0, -2)
    const FirstCheckDigitSum: number = CpfWithoutCheckDigit.reduce((previousValue, currentValue, currentIndex) => previousValue + currentValue * (10 - currentIndex), 0);
    const FirstCheckDigitRemaider: number = FirstCheckDigitSum % 11;
    const FirstCheckDigit: number = (FirstCheckDigitRemaider < 2) ? 0 : 11 - FirstCheckDigitRemaider;
    const SecondCheckDigitSum: number = [...CpfWithoutCheckDigit, FirstCheckDigit].reduce((previousValue, currentValue, currentIndex) => previousValue + currentValue * (11 - currentIndex), 0);
    const SecondCheckDigitRemaider: number = SecondCheckDigitSum % 11
    const SecondCheckDigit: number = (SecondCheckDigitRemaider < 2) ? 0 : 11 - SecondCheckDigitRemaider;
    return parseInt(cpf.at(-2)!) === FirstCheckDigit && parseInt(cpf.at(-1)!) === SecondCheckDigit;
}