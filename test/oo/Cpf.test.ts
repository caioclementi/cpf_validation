import Cpf from "../../src/oo/Cpf";

let cpf: Cpf;

test("Should get Invalid Input: Not a String", function () {
    const data = null as any
	expect(() => cpf = new Cpf(data)).toThrow(new Error("Invalid Input: Not a String"));
});

test("Should get less than 11 chars Invalid Input: String Lenght Incorrect", function () {
	expect(() => cpf = new Cpf('asd')).toThrow(new Error("Invalid Input: String Lenght Incorrect"));
});

test("Should get more than 14 chars Invalid Input: String Lenght Incorrect", function () {
	expect(() => cpf = new Cpf('123456789012345')).toThrow(new Error("Invalid Input: String Lenght Incorrect"));
});

test("Should get Invalid Input: Not Only Valid Chars", function () {
	expect(() => cpf = new Cpf('12345678910$$$')).toThrow(new Error("Invalid Input: Not Only Valid Chars"));
});

test("Should get Invalid Input: All Cpf Number are Equal", function () {
	expect(() => cpf = new Cpf('111.111.111-11')).toThrow(new Error("Invalid Input: All Cpf Number are Equal"));
});

test("Should validate a correct cpf with rest of division more than 2", function () {
    const cpf = new Cpf("608.670.319-13")
    expect(cpf.CpfDigitAlgorithm()).toBeTruthy()
})

test("Should validate a cpf with rest of division less than 2 first digit", function () {
    const cpf = new Cpf('956.313.804-04')
    expect(cpf.CpfDigitAlgorithm()).toBeTruthy()
})

test("Should validate a cpf with rest of division less than 2 second digit", function () {
    const cpf = new Cpf('397.072.642-50')
    expect(cpf.CpfDigitAlgorithm()).toBeTruthy()
})

test("Should validate a incorrect cpf", function () {
    const cpf = new Cpf("111.444.777-05")
    expect(cpf.CpfDigitAlgorithm()).toBeFalsy()
})